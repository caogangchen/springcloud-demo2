package com.melody.order.service.fallback;

import com.melody.order.service.ProductService;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Date: 2022-02-24 11:47
 * @Author: cao_gang
 */
@Service
public class ProductFallbackService implements ProductService {
    @Override
    public String test() {
        return "服务降级";
    }
}
