package com.melody.order.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;

/**
 * @Description:
 * @Date: 2022-02-24 11:15
 * @Author: cao_gang
 */
public class CustomBlockHandler {
    /**
     * 必须为static方法
     * @param exception
     * @return
     */
    public static String  handleException01(BlockException exception){
        return"自定义限流信息1";
    }
    public static String  handleException02(BlockException exception){
        return"自定义限流信息2";
    }
}
