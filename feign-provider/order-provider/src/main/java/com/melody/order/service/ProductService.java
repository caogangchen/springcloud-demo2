package com.melody.order.service;

import com.melody.order.service.fallback.ProductFallbackService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Description: 接口描述
 * @Author: cao_gang
 * @Date: 2021-09-13 13:36
 */
@FeignClient(value = "product-service",fallback = ProductFallbackService.class)
//@FeignClient(value = "product-service",configuration = FeignConfiguration.class)
public interface ProductService {
    @GetMapping("/product/test")
    String test();
}
