package com.melody.order.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.melody.order.handler.CustomBlockHandler;
import com.melody.order.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 类描述
 * @Author: cao_gang
 * @Date: 2021-09-13 13:33
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    private final ProductService productService;

    public OrderController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/test")
    @SentinelResource(value = "test",blockHandler = "handleException01",blockHandlerClass = CustomBlockHandler.class)
    public String test() {
        final String test = productService.test();
        System.out.println(test);
        return "测试";
    }
}
