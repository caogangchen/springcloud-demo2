package com.example.product.global;

import java.io.Serializable;

/**
 * @Description: 业务代码接口
 * @Date: 2021-06-15 22:33
 * @Author: cao_gang
 */
public interface IResultCode extends Serializable {

    /**
     * 消息
     *
     * @return String
     */
    String getMessage();

    /**
     * 状态码
     *
     * @return int
     */
    String getCode();

}

