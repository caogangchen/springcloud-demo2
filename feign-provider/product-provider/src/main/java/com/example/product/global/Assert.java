package com.example.product.global;


/**
 * @Description: 断言处理类，抛出各种API异常
 * @Date: 2021-06-15 22:35
 * @Author: cao_gang
 */
public class Assert {

    public static void fail(String message) {
        throw new MokaException(message);
    }

    public static void fail(String code,String message) {
        throw new MokaException(code,message);
    }

    public static void fail(IResultCode resultCode) {
        throw new MokaException(resultCode.getCode(),resultCode.getMessage());
    }
}
