package com.example.product.config;


import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 该类为Feign的配置类
 * @Author: cao_gang
 * @Date: 2022-01-12 15:13
 */
@Configuration
public class FeignConfiguration {

    /**
     * 日志级别
     *
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
//    /**
//     * 将契约改为feign原生的默认契约。这样就可以使用feign自带的注解了。
//     * @return 默认的feign契约
//     */
//    @Bean
//    public Contract feignContract() {
//        return new feign.Contract.Default();
//    }
}
