package com.example.product.global;

/**
 * @Description: 自定义异常
 * @Date: 2021-06-15 22:35
 * @Author: cao_gang
 */
public class MokaException extends RuntimeException {

    private String code;


    public MokaException(String message) {
        super(message);
    }
    public MokaException(String code,String message) {
        super(message);
        this.code=code;
    }

    public MokaException(Throwable cause) {
        super(cause);
    }

    public MokaException(String message, Throwable cause) {
        super(message, cause);
    }

    public String getCode() {
        return code;
    }
}
