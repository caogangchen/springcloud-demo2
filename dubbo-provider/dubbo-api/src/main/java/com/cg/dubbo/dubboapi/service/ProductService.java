package com.cg.dubbo.dubboapi.service;


import com.cg.dubbo.dubboapi.entity.Product;

import java.util.List;

public interface ProductService {


	List<Product> list();


	Product getById(Integer id);

}