package com.cg.dubbo.order.controller;

import com.cg.dubbo.dubboapi.entity.Product;
import com.cg.dubbo.dubboapi.service.ProductService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: 类描述
 * @Author: cao_gang
 * @Date: 2021-09-13 13:33
 */
@RestController
@RequestMapping("/order")
public class OrderController {


    @DubboReference
    private ProductService productService;

    @GetMapping("/test")
    public List<Product> test() {

        final List<Product> list = productService.list();
        return list;
    }
}
