package com.cg.dubbo.product.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 类描述
 * @Author: cao_gang
 * @Date: 2021-09-13 13:38
 */
@RestController
@RequestMapping("/product")
@Slf4j
public class ProductController {

    @GetMapping("/test")
    public String test() {
        log.info("test");
        return "测试";
    }
}
