package com.cg.dubbo.dubboapi.service.impl;

import com.cg.dubbo.dubboapi.entity.Product;
import com.cg.dubbo.dubboapi.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.ArrayList;
import java.util.List;

@DubboService
@Slf4j
public class ProductServiceImpl implements ProductService {

    @Override
    public List<Product> list() {
        log.info("查询订单列表");
        List<Product> products = new ArrayList<>();
        products.add(new Product(1, "cg"));
        products.add(new Product(2, "xy"));
        return products;
    }

    @Override
    public Product getById(Integer id) {
        return new Product(1, "XX");
    }

}