package com.cg.dubbo.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@SpringBootApplication
public class DubboProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboProductApplication.class, args);
    }

    /**
     * nacos
     * Map(namespace,Map(group:serviceName,service))
     * namespace:隔离作用 如:dev,prod
     * group:serviceName: 通常用于配置中心，配置中心隔离作用
     * service:就是微服务 order product
     */

}
